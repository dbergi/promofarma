<?php


namespace App\Application\Command\Cart;


use App\Domain\Cart\ValueObject\CartProduct;
use Ramsey\Uuid\UuidInterface;

class AddProductToCartCommand
{
    private UuidInterface $cartId;

    private UuidInterface $productId;

    private int $quantity;

    public function __construct(
        UuidInterface $cartId,
        UuidInterface $cartProduct,
        int $quantity
    )
    {
        $this->cartId = $cartId;
        $this->productId = $cartProduct;
        $this->quantity = $quantity;
    }

    public function cartId(): UuidInterface
    {
        return $this->cartId;
    }

    public function productId(): UuidInterface
    {
        return $this->productId;
    }

    public function quantity(): int
    {
        return $this->quantity;
    }
}