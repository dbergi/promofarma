<?php


namespace App\Application\Command\Cart;


use App\Application\Command\CommandHandlerInterface;
use App\Domain\Cart\Cart;
use App\Domain\Cart\ValueObject\CartProduct;
use App\Domain\Product\Product;
use App\Domain\Shared\ValueObject\Status;
use App\Infrastructure\Cart\Repository\CartStore;
use App\Infrastructure\Product\Repository\ProductStore;
use Broadway\CommandHandling\SimpleCommandHandler;
use Broadway\Domain\AggregateRoot;
use Broadway\Repository\AggregateNotFoundException;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class CartCommandHandler extends SimpleCommandHandler implements CommandHandlerInterface
{
    private CartStore $cartStore;

    private ProductStore $productStore;

    public function __construct(
        CartStore $cartStore,
        ProductStore $productStore
    )
    {
        $this->cartStore = $cartStore;
        $this->productStore = $productStore;
    }

    public function handleAddProductToCartCommand(AddProductToCartCommand $command): void
    {
        try {
            $cart = $this->getItem('Cart', $command->cartId());
        } catch (AggregateNotFoundException $e) {
            $cart = Cart::create($command->cartId());
        }

        /** @var Product $product */
        $product = $this->getItem('Product', $command->productId());

        $cartProduct = CartProduct::fromArray([
            'productId' => $command->productId(),
            'quantity' => $command->quantity(),
            'unitCost' => $product->cost()
        ]);

        $cart->addProduct($cartProduct);

        $this->cartStore->store($cart);
    }

    public function handleChangeProductQuantityCommand(ChangeProductQuantityCommand $command): void
    {
        /** @var Cart $cart */
        $cart = $this->getItem('Cart', $command->cartId());

        /** @var Product $product */
        $product = $this->getItem('Product', $command->productId());

        $cartProduct = CartProduct::fromArray([
            'productId' => $command->productId(),
            'quantity' => $command->quantity(),
            'unitCost' => $product->cost()
        ]);

        if (!isset($cart->products()[$command->productId()->toString()])) {
            throw new \Exception('Illegal productId. It may have been removed.');
        }

        $cart->changeProductQuantity($cartProduct, $command->increase());

        $this->cartStore->store($cart);
    }

    public function handleRemoveProductFromCartCommand(RemoveProductFromCartCommand $command): void
    {
        /** @var Cart $cart */
        $cart = $this->getItem('Cart', $command->cartId());

        /** @var Product $product */
        $product = $this->getItem('Product', $command->productId());

        $cartProduct = CartProduct::fromArray([
            'productId' => $command->productId(),
            'quantity' => 0,
            'unitCost' => $product->cost()
        ]);

        if (!isset($cart->products()[$command->productId()->toString()])) {
            throw new \Exception('Illegal productId. Product may have been removed from cart.');
        }

        $cart->removeProduct($cartProduct);

        $this->cartStore->store($cart);
    }

    public function handleDeleteCartCommand(DeleteCartCommand $command): void
    {
        /** @var Cart $cart */
        $cart = $this->getItem('Cart', $command->cartId());

        foreach ($cart->products() as $product) {
            $cart->removeProduct(CartProduct::fromArray($product));
        }

        $cart->deleteCart();

        $this->cartStore->store($cart);
    }

    public function handleConfirmCartCommand(ConfirmCartCommand $command): void
    {
        /** @var Cart $cart */
        $cart = $this->getItem('Cart', $command->cartId());

        $cart->confirmCart();

        $this->cartStore->store($cart);
    }

    private function getItem(string $className, UuidInterface $id): AggregateRoot
    {
        $store = strtolower($className) . 'Store';
        $item = $this->$store->get($id);

        if (Status::INACTIVE()->equals($item->status())) {
            throw new ResourceNotFoundException(sprintf('%s deleted. Change Id.', $className));
        }

        return $item;
    }
}