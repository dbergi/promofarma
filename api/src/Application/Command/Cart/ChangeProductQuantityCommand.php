<?php


namespace App\Application\Command\Cart;


use Ramsey\Uuid\UuidInterface;

class ChangeProductQuantityCommand
{
    private UuidInterface $cartId;

    private UuidInterface $productId;

    private int $quantity;

    private bool $increase;

    public function __construct(
        UuidInterface $cartId,
        UuidInterface $productId,
        int $quantity,
        bool $increase
    )
    {
        $this->cartId = $cartId;
        $this->productId = $productId;
        $this->quantity = $quantity;
        $this->increase = $increase;
    }

    public function cartId(): UuidInterface
    {
        return $this->cartId;
    }

    public function productId(): UuidInterface
    {
        return $this->productId;
    }

    public function quantity(): int
    {
        return $this->quantity;
    }

    public function increase(): bool
    {
        return $this->increase;
    }
}