<?php


namespace App\Application\Command\Cart;


use Ramsey\Uuid\UuidInterface;

class ConfirmCartCommand
{
    private UuidInterface $cartId;

    public function __construct(
        UuidInterface $cartId
    )
    {
        $this->cartId = $cartId;
    }

    public function cartId(): UuidInterface
    {
        return $this->cartId;
    }
}