<?php


namespace App\Application\Command\Cart;


use App\Domain\Cart\ValueObject\CartProduct;
use Ramsey\Uuid\UuidInterface;

class RemoveProductFromCartCommand
{
    private UuidInterface $cartId;

    private UuidInterface $productId;

    public function __construct(
        UuidInterface $cartId,
        UuidInterface $productId
    )
    {
        $this->cartId = $cartId;
        $this->productId = $productId;
    }

    public function cartId(): UuidInterface
    {
        return $this->cartId;
    }

    public function productId(): UuidInterface
    {
        return $this->productId;
    }
}