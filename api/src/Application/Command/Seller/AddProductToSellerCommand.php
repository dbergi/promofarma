<?php


namespace App\Application\Command\Seller;


use Ramsey\Uuid\UuidInterface;

class AddProductToSellerCommand
{
    private UuidInterface $sellerId;

    private UuidInterface $productId;

    private string $productName;

    private float $cost;

    public function __construct(UuidInterface $sellerId, UuidInterface $productId, string $productName, float $cost)
    {
        $this->sellerId = $sellerId;
        $this->productId = $productId;
        $this->productName = $productName;
        $this->cost = $cost;
    }

    public function sellerId(): UuidInterface
    {
        return $this->sellerId;
    }

    public function productId(): UuidInterface
    {
        return $this->productId;
    }

    public function productName(): string
    {
        return $this->productName;
    }

    public function cost(): float
    {
        return $this->cost;
    }
}