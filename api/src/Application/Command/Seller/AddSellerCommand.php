<?php


namespace App\Application\Command\Seller;


use Ramsey\Uuid\UuidInterface;

class AddSellerCommand
{
    private UuidInterface $id;

    private string $name;

    public function __construct(UuidInterface $id, string $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }
}