<?php


namespace App\Application\Command\Seller;


use Ramsey\Uuid\UuidInterface;

class RemoveProductFromSellerCommand
{
    private UuidInterface $sellerId;

    private UuidInterface $productId;

    public function __construct(UuidInterface $sellerId, UuidInterface $productId)
    {
        $this->sellerId = $sellerId;
        $this->productId = $productId;
    }

    public function sellerId(): UuidInterface
    {
        return $this->sellerId;
    }

    public function productId(): UuidInterface
    {
        return $this->productId;
    }
}