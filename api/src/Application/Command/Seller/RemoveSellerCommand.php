<?php


namespace App\Application\Command\Seller;


use Ramsey\Uuid\UuidInterface;

class RemoveSellerCommand
{
    private UuidInterface $id;

    public function __construct(UuidInterface $id)
    {
        $this->id = $id;
    }

    public function id(): UuidInterface
    {
        return $this->id;
    }

}