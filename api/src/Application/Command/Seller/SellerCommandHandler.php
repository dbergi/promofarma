<?php


namespace App\Application\Command\Seller;


use App\Application\Command\CommandHandlerInterface;
use App\Domain\Product\Product;
use App\Domain\Seller\Seller;
use App\Domain\Shared\Query\Exception\NotFoundException;
use App\Domain\Shared\ValueObject\Status;
use App\Infrastructure\Product\Repository\ProductStore;
use App\Infrastructure\Seller\Repository\SellerStore;
use Broadway\CommandHandling\SimpleCommandHandler;
use Broadway\Repository\AggregateNotFoundException;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class SellerCommandHandler extends SimpleCommandHandler implements CommandHandlerInterface
{
    private SellerStore $sellerStore;
    private ProductStore $productStore;

    public function __construct(
        SellerStore $sellerStore,
        ProductStore $productStore
    )
    {
        $this->sellerStore = $sellerStore;
        $this->productStore = $productStore;
    }

    public function handleAddSellerCommand(AddSellerCommand $command): void
    {
        try {
            $seller = $this->sellerStore->get($command->id());

            if (Status::INACTIVE()->equals($seller->status())){
                $seller->reactivateSeller();
            }
        } catch (AggregateNotFoundException $exception) {
            $seller = Seller::create($command->id(), $command->name());
        }

        $this->sellerStore->store($seller);
    }

    public function handleRemoveSellerCommand(RemoveSellerCommand $command): void
    {
        $seller = $this->sellerStore->get($command->id());

        $sellerProducts = $seller->products();

        foreach ($sellerProducts as $productId) {
            $product = $this->productStore->get(Uuid::fromString($productId));

            $seller->removeProduct($product);
            $this->sellerStore->store($seller);

            $product->removeProduct();
            $this->productStore->store($product);
        }

        $seller->deleteSeller();
        $this->sellerStore->store($seller);
    }

    public function handleAddProductToSellerCommand(AddProductToSellerCommand $command): void
    {
        $seller = $this->sellerStore->get($command->sellerId());

        if (Status::INACTIVE()->equals($seller->status())) {
            throw new NotFoundException('Seller is deleted.');
        }

        try {
            $product = $this->productStore->get($command->productId());

            if (Status::INACTIVE()->equals($product->status())){
                $product->reactivateProduct();
                $this->productStore->store($product);
            }
        } catch (AggregateNotFoundException $exception) {
            $product = Product::create(
                $command->productId(),
                $command->productName(),
                $command->cost(),
                $command->sellerId()
            );

            $this->productStore->store($product);
        }

        $seller->addProduct($product);

        $this->sellerStore->store($seller);
    }

    public function handleRemoveProductFromSellerCommand(RemoveProductFromSellerCommand $command): void
    {
        $seller = $this->sellerStore->get($command->sellerId());
        $product = $this->productStore->get($command->productId());

        $seller->removeProduct($product);
        $this->sellerStore->store($seller);

        $product->removeProduct();
        $this->productStore->store($product);
    }
}