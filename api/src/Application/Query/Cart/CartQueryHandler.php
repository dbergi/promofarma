<?php


namespace App\Application\Query\Cart;


use App\Application\Query\QueryHandlerInterface;
use App\Infrastructure\Cart\Repository\CartStore;

class CartQueryHandler implements QueryHandlerInterface
{
    private CartStore $cartStore;

    public function __construct(
        CartStore $cartStore
    )
    {
        $this->cartStore = $cartStore;
    }

    public function handleGetTotalAmountOfCartQuery(GetTotalAmountOfCartQuery $query): float
    {
        $cartView = $this->cartStore->get($query->cartId());

        return $cartView->getTotalAmountOfCart();
    }
}