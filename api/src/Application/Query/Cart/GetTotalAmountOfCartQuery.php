<?php


namespace App\Application\Query\Cart;


use Ramsey\Uuid\UuidInterface;

class GetTotalAmountOfCartQuery
{
    private UuidInterface $cartId;

    public function __construct(
        UuidInterface $cartId
    )
    {
        $this->cartId = $cartId;
    }

    public function cartId(): UuidInterface
    {
        return $this->cartId;
    }
}