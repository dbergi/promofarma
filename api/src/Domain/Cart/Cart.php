<?php

declare(strict_types=1);

namespace App\Domain\Cart;


use App\Domain\Cart\Event\CartConfirmed;
use App\Domain\Cart\Event\CartCreated;
use App\Domain\Cart\Event\CartDeleted;
use App\Domain\Cart\Event\CartProductChangedQuantity;
use App\Domain\Cart\Event\CartProductRemoved;
use App\Domain\Cart\Event\ProductAddedToCart;
use App\Domain\Cart\ValueObject\CartProduct;
use App\Domain\Shared\ValueObject\DateTime;
use App\Domain\Shared\ValueObject\Status;
use Broadway\EventSourcing\EventSourcedAggregateRoot;
use Ramsey\Uuid\UuidInterface;

class Cart extends EventSourcedAggregateRoot
{

    private UuidInterface $uuid;

    private array $products;

    private DateTime $createdAt;

    private DateTime $updatedAt;

    private Status $status;

    public static function create(
        UuidInterface $uuid
    ): self
    {
        $cart = new self();

        $cart->apply(new CartCreated($uuid, [], DateTime::now(), Status::ACTIVE()));

        return $cart;
    }

    public function getAggregateRootId(): string
    {
        return $this->uuid->toString();
    }

    public function products(): array
    {
        return $this->products;
    }

    public function status(): Status
    {
        return $this->status;
    }

    public function getTotalAmountOfCart(): float
    {
        $totalAmount = 0.0;

        foreach ($this->products as $product) {
            $totalAmount += $product['quantity'] * $product['unitCost'];
        }

        return $totalAmount;
    }

    public function deleteCart(): void
    {
        $this->apply(new CartDeleted($this->uuid, DateTime::now(), Status::INACTIVE()));
    }

    public function addProduct(CartProduct $cartProduct): void
    {
        $this->apply(new ProductAddedToCart($this->uuid, $cartProduct, DateTime::now()));
    }

    public function removeProduct(CartProduct $cartProduct): void
    {
        $this->apply(new CartProductRemoved(
            $this->uuid,
            $cartProduct,
            DateTime::now(),
        ));
    }

    public function changeProductQuantity(CartProduct $cartProduct, bool $increase): void
    {
        $this->apply(new CartProductChangedQuantity($this->uuid, $cartProduct, $increase, DateTime::now()));

        if ($this->products[$cartProduct->toArray()['productId']]['quantity'] === 0) {
            $this->removeProduct($cartProduct);
        }
    }

    public function confirmCart(): void
    {
        $this->apply(new CartConfirmed($this->uuid, DateTime::now(), Status::CONFIRMED()));
    }

    protected function applyCartCreated(CartCreated $event): void
    {
        $this->uuid = $event->uuid;
        $this->products = $event->products;
        $this->createdAt = $event->createdAt;
        $this->status = $event->status;
    }

    protected function applyCartDeleted(CartDeleted $event): void
    {
        $this->uuid = $event->uuid;
        $this->updatedAt = $event->updatedAt;
        $this->status = $event->status;
    }

    protected function applyProductAddedToCart(ProductAddedToCart $event): void
    {
        $cartProduct = $event->cartProduct->toArray();
        $productId = $cartProduct['productId'];

        if (isset($this->products[$productId])) {
            $this->products[$productId]['quantity'] += $cartProduct['quantity'];
        } else {
            $this->products[$productId] = $cartProduct;
        }

        $this->updatedAt = $event->updatedAt;
    }

    protected function applyCartProductChangedQuantity(CartProductChangedQuantity $event): void
    {
        $cartProduct = $event->cartProduct->toArray();
        $productId = $cartProduct['productId'];

        if ($event->increase) {
            $this->products[$productId]['quantity'] += $cartProduct['quantity'];
        } elseif ($cartProduct['quantity'] <= $this->products[$productId]['quantity']) {
            $this->products[$productId]['quantity'] -= $cartProduct['quantity'];
        } else {
            $this->products[$productId]['quantity'] = 0;
        }

        $this->updatedAt = $event->updatedAt;
    }

    protected function applyCartProductRemoved(CartProductRemoved $event): void
    {
        $cartProduct = $event->cartProduct->toArray();

        unset($this->products[$cartProduct['productId']]);
        $this->updatedAt = $event->updatedAt;
    }

    protected function applyCartConfirmed(CartConfirmed $event): void
    {
        $this->status = $event->status;
        $this->updatedAt = $event->updatedAt;
    }
}