<?php


namespace App\Domain\Cart\Event;


use App\Domain\Shared\ValueObject\DateTime;
use App\Domain\Shared\ValueObject\Status;
use Assert\Assertion;
use Broadway\Serializer\Serializable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class CartConfirmed implements Serializable
{
    public UuidInterface $uuid;

    public DateTime $updatedAt;

    public Status $status;

    public function __construct(
        UuidInterface $uuid,
        DateTime $updatedAt,
        Status $status
    )
    {
        $this->uuid = $uuid;
        $this->updatedAt = $updatedAt;
        $this->status = $status;
    }

    /**
     * @inheritDoc
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'uuid');
        Assertion::keyExists($data, 'status');

        return new self(
            Uuid::fromString($data['uuid']),
            DateTime::fromString($data['updated_at']),
            new Status($data['status'])
        );
    }

    public function serialize(): array
    {
        return [
            'uuid' => $this->uuid->toString(),
            'updated_at' => $this->updatedAt->toString(),
            'status' => $this->status
        ];
    }
}