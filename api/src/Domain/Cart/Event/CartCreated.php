<?php


namespace App\Domain\Cart\Event;


use App\Domain\Shared\ValueObject\DateTime;
use App\Domain\Shared\ValueObject\Status;
use Assert\Assertion;
use Broadway\Serializer\Serializable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class CartCreated implements Serializable
{

    public UuidInterface $uuid;

    public array $products;

    public DateTime $createdAt;

    public Status $status;

    public function __construct(
        UuidInterface $uuid,
        array $products,
        DateTime $createdAt,
        Status $status
    )
    {
        $this->uuid = $uuid;
        $this->products = $products;
        $this->createdAt = $createdAt;
        $this->status = $status;
    }

    /**
     * @inheritDoc
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'uuid');
        Assertion::keyExists($data, 'products');
        Assertion::keyExists($data, 'status');

        return new self(
            Uuid::fromString($data['uuid']),
            $data['products'],
            DateTime::fromString($data['created_at']),
            new Status($data['status'])
        );
    }

    public function serialize(): array
    {
        return [
            'uuid' => $this->uuid->toString(),
            'products' => $this->products,
            'created_at' => $this->createdAt->toString(),
            'status' => $this->status
        ];
    }
}