<?php


namespace App\Domain\Cart\Event;


use App\Domain\Cart\ValueObject\CartProduct;
use App\Domain\Shared\ValueObject\DateTime;
use Assert\Assertion;
use Broadway\Serializer\Serializable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class CartProductChangedQuantity implements Serializable
{
    public UuidInterface $uuid;

    public CartProduct $cartProduct;

    public bool $increase;

    public DateTime $updatedAt;

    public function __construct(UuidInterface $uuid, CartProduct $cartProduct, bool $increase, DateTime $updatedAt)
    {
        $this->uuid = $uuid;
        $this->cartProduct = $cartProduct;
        $this->increase = $increase;
        $this->updatedAt = $updatedAt;
    }

    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'uuid');
        Assertion::keyExists($data, 'cartProduct');
        Assertion::keyExists($data, 'increase');

        return new self(
            Uuid::fromString($data['uuid']),
            CartProduct::fromArray($data['cartProduct']),
            $data['increase'],
            DateTime::fromString($data['updated_at'])
        );
    }

    public function serialize(): array
    {
        return [
            'uuid' => $this->uuid->toString(),
            'cartProduct' => $this->cartProduct->toArray(),
            'increase' => $this->increase,
            'updated_at' => $this->updatedAt->toString()
        ];
    }
}