<?php


namespace App\Domain\Cart\Event;


use App\Domain\Cart\Cart;
use App\Domain\Cart\ValueObject\CartProduct;
use App\Domain\Product\Product;
use App\Domain\Shared\ValueObject\DateTime;
use App\Domain\Shared\ValueObject\Status;
use Assert\Assertion;
use Broadway\Serializer\Serializable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class ProductAddedToCart implements Serializable
{
    public UuidInterface $uuid;

    public CartProduct $cartProduct;

    public DateTime $updatedAt;

    public function __construct(
        UuidInterface $uuid,
        CartProduct $cartProduct,
        DateTime $updatedAt
    )
    {
        $this->uuid = $uuid;
        $this->cartProduct = $cartProduct;
        $this->updatedAt = $updatedAt;
    }

    /**
     * @inheritDoc
     */
    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'uuid');
        Assertion::keyExists($data, 'cartProduct');

        return new self(
            Uuid::fromString($data['uuid']),
            CartProduct::fromArray($data['cartProduct']),
            DateTime::fromString($data['updated_at'])
        );
    }

    public function serialize(): array
    {
        return [
            'uuid' => $this->uuid->toString(),
            'cartProduct' => $this->cartProduct->toArray(),
            'updated_at' => $this->updatedAt->toString()
        ];
    }
}