<?php


namespace App\Domain\Cart\Repository;


use App\Infrastructure\Cart\Query\Projections\CartView;
use Ramsey\Uuid\UuidInterface;

interface CartReadModelRepositoryInterface
{
    public function oneById(UuidInterface $id): CartView;

    public function add(CartView $seller): void;

    public function delete(CartView $seller): void;
}