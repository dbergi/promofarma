<?php


namespace App\Domain\Cart\ValueObject;


use App\Domain\Product\Product;
use Broadway\EventSourcing\EventSourcedAggregateRoot;

class CartProduct
{
    private string $productId;

    private int $quantity;

    private float $unitCost;

    public static function fromString(string $product): self
    {
        $product = json_decode($product, true);

        return self::fromArray($product);
    }

    public static function fromArray(array $product): self
    {
        $cartProduct = new self();

        $cartProduct->productId = $product['productId'];
        $cartProduct->quantity = $product['quantity'];
        $cartProduct->unitCost = $product['unitCost'];

        return $cartProduct;
    }

    public function __toString(): string
    {
        return json_encode([
            "productId" => $this->productId,
            "quantity" => $this->quantity,
            "unitCost" => $this->unitCost
        ]);
    }

    public function toArray(): array
    {
        return [
            "productId" => $this->productId,
            'quantity' => $this->quantity,
            'unitCost' => $this->unitCost
        ];
    }
}