<?php


namespace App\Domain\Product\Event;


use App\Domain\Shared\ValueObject\DateTime;
use App\Domain\Shared\ValueObject\Status;
use Assert\Assertion;
use Broadway\Serializer\Serializable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class ProductReactivated implements Serializable
{
    public UuidInterface $uuid;

    public string $name;

    public float $cost;

    public UuidInterface $sellerId;

    public DateTime $createdAt;

    public DateTime $updatedAt;

    public Status $status;

    public function __construct(
        UuidInterface $uuid,
        UuidInterface $sellerId,
        string $name,
        float $cost,
        DateTime $createdAt,
        DateTime $updatedAt,
        Status $status
    )
    {
        $this->uuid = $uuid;
        $this->sellerId = $sellerId;
        $this->name = $name;
        $this->cost = $cost;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->status = $status;
    }

    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'uuid');
        Assertion::keyExists($data, 'sellerId');
        Assertion::keyExists($data, 'name');
        Assertion::keyExists($data, 'cost');
        Assertion::keyExists($data, 'status');

        return new self(
            Uuid::fromString($data['uuid']),
            Uuid::fromString($data['sellerId']),
            $data['name'],
            $data['cost'],
            DateTime::fromString($data['created_at']),
            DateTime::fromString($data['updated_at']),
            new Status($data['status']),
        );
    }

    public function serialize(): array
    {
        return [
            'uuid' => $this->uuid->toString(),
            'sellerId' => $this->sellerId->toString(),
            'name' => $this->name,
            'cost' => $this->cost,
            'created_at' => $this->createdAt->toString(),
            'updated_at' => $this->updatedAt->toString(),
            'status' => $this->status
        ];
    }
}