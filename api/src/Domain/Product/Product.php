<?php

declare(strict_types=1);

namespace App\Domain\Product;


use App\Domain\Product\Event\ProductCreated;
use App\Domain\Product\Event\ProductReactivated;
use App\Domain\Product\Event\ProductRemoved;
use App\Domain\Seller\Event\ProductAddedToSeller;
use App\Domain\Seller\Event\ProductRemovedFromSeller;
use App\Domain\Seller\Event\SellerDeleted;
use App\Domain\Shared\ValueObject\DateTime;
use App\Domain\Shared\ValueObject\Status;
use Broadway\EventSourcing\EventSourcedAggregateRoot;
use Ramsey\Uuid\UuidInterface;

class Product extends EventSourcedAggregateRoot
{

    private UuidInterface $uuid;

    private string $name;

    private float $cost;

    private UuidInterface $seller;

    private DateTime $createdAt;

    private DateTime $updatedAt;

    private Status $status;

    public static function create(
        UuidInterface $uuid,
        string $name,
        float $cost,
        UuidInterface $sellerId
    ): self
    {
        $product = new self();

        $product->apply(new ProductCreated($uuid, $sellerId, $name, $cost, DateTime::now(), Status::ACTIVE()));

        return $product;
    }

    public function id(): UuidInterface
    {
        return $this->uuid;
    }

    public function name(): string
    {
       return $this->name;
    }

    public function cost(): float
    {
        return $this->cost;
    }

    public function status(): Status
    {
        return $this->status;
    }

    public function sellerId(): UuidInterface
    {
        return $this->seller;
    }

    public function getAggregateRootId(): string
    {
        return $this->uuid->toString();
    }

    public function reactivateProduct(): void
    {
        $this->apply(new ProductReactivated($this->uuid, $this->seller, $this->name, $this->cost, $this->createdAt, DateTime::now(), Status::ACTIVE()));
    }

    public function removeProduct() {
        $this->apply(new ProductRemoved($this->uuid, DateTime::now(), Status::INACTIVE()));
    }

    protected function applyProductCreated(ProductCreated $event): void
    {
        $this->uuid = $event->uuid;
        $this->name = $event->name;
        $this->cost = $event->cost;
        $this->seller = $event->sellerId;
        $this->createdAt = $event->createdAt;
        $this->status = $event->status;
    }

    protected function applyProductReactivated(ProductReactivated $event): void
    {
        $this->updatedAt = $event->updatedAt;
        $this->status = $event->status;
    }

    protected function applyProductRemoved(ProductRemoved $event): void
    {
        $this->status = $event->status;
        $this->updatedAt = $event->updatedAt;
    }

    public function applySellerDeleted(SellerDeleted $event): void
    {
        $this->status = $event->status;
        $this->updatedAt = $event->updatedAt;
    }
}