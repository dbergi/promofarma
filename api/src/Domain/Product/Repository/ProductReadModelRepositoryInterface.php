<?php


namespace App\Domain\Product\Repository;


use App\Infrastructure\Product\Query\Projections\ProductView;
use Ramsey\Uuid\UuidInterface;

interface ProductReadModelRepositoryInterface
{
    public function oneById(UuidInterface $id): ProductView;

    public function add(ProductView $seller): void;

    public function delete(ProductView $seller): void;
}