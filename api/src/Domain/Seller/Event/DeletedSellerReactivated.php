<?php


namespace App\Domain\Seller\Event;


use App\Domain\Shared\Exception\DateTimeException;
use App\Domain\Shared\ValueObject\DateTime;
use App\Domain\Shared\ValueObject\Status;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class DeletedSellerReactivated implements Serializable
{
    public UuidInterface $id;

    public string $name;

    public Collection $products;

    public DateTime $createdAt;

    public DateTime $updatedAt;

    public Status $status;

    public function __construct(UuidInterface $id, string $name, Collection $products, DateTime $createdAt, DateTime $updatedAt, Status $status)
    {
        $this->id = $id;
        $this->name = $name;
        $this->products = $products;
        $this->createdAt = $createdAt;
        $this->updatedAt = $updatedAt;
        $this->status = $status;
    }

    /**
     * @param array $data
     * @return static
     * @throws DateTimeException
     * @throws AssertionFailedException
     */
    public static function deserialize(array $data): self
    {
        Assertion::keyExists($data, 'id');
        Assertion::keyExists($data, 'name');
        Assertion::keyExists($data, 'products');
        Assertion::keyExists($data, 'status');

        return new self(
            Uuid::fromString($data['id']),
            $data['name'],
            new ArrayCollection($data['products']),
            DateTime::fromString($data['created_at']),
            DateTime::fromString($data['updated_at']),
            new Status($data['status'])
        );
    }

    public function serialize(): array
    {
        return [
            'id'    => $this->id->toString(),
            'name' => $this->name,
            'products' => $this->products,
            'created_at' => $this->createdAt->toString(),
            'updated_at' => $this->updatedAt->toString(),
            'status' => $this->status
        ];
    }
}