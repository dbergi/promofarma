<?php


namespace App\Domain\Seller\Event;


use App\Domain\Shared\ValueObject\DateTime;
use Assert\Assertion;
use Broadway\Serializer\Serializable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class ProductAddedToSeller implements Serializable
{
    public UuidInterface $sellerId;

    public UuidInterface $productId;

    public DateTime $updatedAt;

    public function __construct(
        UuidInterface $sellerId,
        UuidInterface $productId,
        DateTime $updatedAt
    ) {
        $this->sellerId = $sellerId;
        $this->productId = $productId;
        $this->updatedAt = $updatedAt;
    }

    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'sellerId');
        Assertion::keyExists($data, 'productId');

        return new self(
            Uuid::fromString($data['sellerId']),
            Uuid::fromString($data['productId']),
            DateTime::fromString($data['updated_at']),
        );
    }

    public function serialize(): array
    {
        return [
            'sellerId'    => $this->sellerId->toString(),
            'productId'    => $this->productId->toString(),
            'updated_at' => $this->updatedAt->toString(),
        ];
    }
}