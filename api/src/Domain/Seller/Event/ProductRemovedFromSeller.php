<?php


namespace App\Domain\Seller\Event;


use App\Domain\Shared\ValueObject\DateTime;
use App\Domain\Shared\ValueObject\Status;
use Assert\Assertion;
use Broadway\Serializer\Serializable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class ProductRemovedFromSeller implements Serializable
{
    public UuidInterface $sellerId;

    public UuidInterface $productId;

    public DateTime $updatedAt;

    public Status $status;

    public function __construct(UuidInterface $sellerId, UuidInterface $productId, DateTime $updatedAt, Status $status)
    {
        $this->sellerId = $sellerId;
        $this->productId = $productId;
        $this->updatedAt = $updatedAt;
        $this->status = $status;
    }

    public static function deserialize(array $data)
    {
        Assertion::keyExists($data, 'productId');
        Assertion::keyExists($data, 'sellerId');
        Assertion::keyExists($data, 'status');

        return new self(
            Uuid::fromString($data['sellerId']),
            Uuid::fromString($data['productId']),
            DateTime::fromString($data['updated_at']),
            new Status($data['status'])
        );
    }

    public function serialize(): array
    {
        return [
            'sellerId'    => $this->sellerId->toString(),
            'productId' => $this->productId->toString(),
            'updated_at' => $this->updatedAt->toString(),
            'status' => $this->status
        ];
    }
}