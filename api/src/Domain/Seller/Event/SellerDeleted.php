<?php


namespace App\Domain\Seller\Event;


use App\Domain\Shared\Exception\DateTimeException;
use App\Domain\Shared\ValueObject\DateTime;
use App\Domain\Shared\ValueObject\Status;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Broadway\Serializer\Serializable;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class SellerDeleted implements Serializable
{
    public UuidInterface $id;

    public DateTime $updatedAt;

    public Status $status;

    public function __construct(UuidInterface $id, DateTime $updatedAt, Status $status)
    {
        $this->id = $id;
        $this->updatedAt = $updatedAt;
        $this->status = $status;
    }

    /**
     * @param array $data
     * @return static
     * @throws DateTimeException
     * @throws AssertionFailedException
     */
    public static function deserialize(array $data): self
    {
        Assertion::keyExists($data, 'id');
        Assertion::keyExists($data, 'status');

        return new self(
            Uuid::fromString($data['id']),
            DateTime::fromString($data['updated_at']),
            new Status($data['status'])
        );
    }

    public function serialize(): array
    {
        return [
            'id'    => $this->id->__toString(),
            'updated_at' => $this->updatedAt->toString(),
            'status' => $this->status
        ];
    }
}