<?php


namespace App\Domain\Seller\Repository;


use App\Infrastructure\Seller\Query\Projections\SellerView;
use Ramsey\Uuid\UuidInterface;

interface SellerReadModelRepositoryInterface
{
    public function oneById(UuidInterface $id): SellerView;

    public function add(SellerView $seller): void;

    public function delete(SellerView $seller): void;
}