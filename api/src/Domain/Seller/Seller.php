<?php

declare(strict_types=1);

namespace App\Domain\Seller;


use App\Domain\Product\Product;
use App\Domain\Seller\Event\DeletedSellerReactivated;
use App\Domain\Seller\Event\ProductAddedToSeller;
use App\Domain\Seller\Event\ProductRemovedFromSeller;
use App\Domain\Seller\Event\SellerAdded;
use App\Domain\Seller\Event\SellerDeleted;
use App\Domain\Shared\Event\StatusChanged;
use App\Domain\Shared\Query\Exception\NotFoundException;
use App\Domain\Shared\ValueObject\DateTime;
use App\Domain\Shared\ValueObject\Status;
use Broadway\EventSourcing\EventSourcedAggregateRoot;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class Seller extends EventSourcedAggregateRoot
{
    private UuidInterface $uuid;

    private string $name;

    private Collection $products;

    private DateTime $createdAt;

    private DateTime $updatedAt;

    private Status $status;

    public static function create(
        UuidInterface $id,
        string $name
    ): self
    {
        $seller = new self();

        $seller->apply(new SellerAdded($id, $name, new ArrayCollection(), DateTime::now(), Status::ACTIVE()));

        return $seller;
    }

    public function getAggregateRootId(): string
    {
        return (String) $this->uuid;
    }

    public function id(): UuidInterface
    {
        return $this->uuid;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function products(): Collection
    {
        return $this->products;
    }

    public function status(): Status
    {
        return $this->status;
    }

    public function reactivateSeller(): void
    {
        $this->apply(new DeletedSellerReactivated($this->uuid, $this->name, new ArrayCollection(), $this->createdAt, DateTime::now(), Status::ACTIVE()));
    }

    public function deleteSeller(): void
    {
        $this->apply(new SellerDeleted($this->uuid, DateTime::now(), Status::INACTIVE()));
    }

    public function addProduct(Product $product): void
    {
        $this->apply(new ProductAddedToSeller($this->id(), $product->id(), DateTime::now()));
    }

    public function removeProduct(Product $product)
    {
        $this->apply(new ProductRemovedFromSeller(
            $this->uuid,
            $product->id(),
            DateTime::now(),
            Status::INACTIVE()
        ));
    }

    protected function applySellerAdded(SellerAdded $event): void
    {
        $this->uuid = $event->id;
        $this->name = $event->name;
        $this->products = $event->products;
        $this->createdAt = $event->createdAt;
        $this->status = $event->status;
    }

    protected function applyDeletedSellerReactivated(DeletedSellerReactivated $event): void
    {
        $this->createdAt = $event->createdAt;
        $this->status = $event->status;
    }

    protected function applySellerDeleted(SellerDeleted $event): void
    {
        $this->status = $event->status;
        $this->updatedAt = $event->updatedAt;
    }

    protected function applyProductAddedToSeller(ProductAddedToSeller $event): void
    {
        $this->products[$event->productId->toString()] = $event->productId->toString();
        $this->updatedAt = $event->updatedAt;
    }

    protected function applyProductRemovedFromSeller(ProductRemovedFromSeller $event): void
    {
        if (!isset($this->products[$event->productId->toString()])) {
            throw new NotFoundException('Product not linked to seller');
        }

        unset($this->products[$event->productId->toString()]);
        $this->updatedAt = $event->updatedAt;
    }
}