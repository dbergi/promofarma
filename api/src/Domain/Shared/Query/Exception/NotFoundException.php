<?php


namespace App\Domain\Shared\Query\Exception;


class NotFoundException extends \Exception
{
    public function __construct(string $message = null)
    {
        parent::__construct($message ?? 'Resource not found', 404);
    }
}