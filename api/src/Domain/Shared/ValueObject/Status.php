<?php


namespace App\Domain\Shared\ValueObject;


use MyCLabs\Enum\Enum;

/**
 * @method static Status INACTIVE()
 * @method static Status ACTIVE()
 * @method static Status CONFIRMED()
 */
class Status extends Enum
{
    private const INACTIVE = 0;
    private const ACTIVE = 1;
    private const CONFIRMED = 2;
}