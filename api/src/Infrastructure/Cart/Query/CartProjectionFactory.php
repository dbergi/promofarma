<?php

declare(strict_types=1);

namespace App\Infrastructure\Cart\Query;

use App\Application\Command\Cart\ChangeProductQuantityCommand;
use App\Domain\Cart\Event\CartConfirmed;
use App\Domain\Cart\Event\CartCreated;
use App\Domain\Cart\Event\CartDeleted;
use App\Domain\Cart\Event\CartProductChangedQuantity;
use App\Domain\Cart\Event\CartProductRemoved;
use App\Domain\Cart\Event\ProductAddedToCart;
use App\Infrastructure\Cart\Query\Mysql\MysqlCartReadModelRepository;
use App\Infrastructure\Cart\Query\Projections\CartView;
use Broadway\ReadModel\Projector;

class CartProjectionFactory extends Projector
{
    private MysqlCartReadModelRepository $repository;

    public function __construct(
        MysqlCartReadModelRepository $repository
    ) {
        $this->repository = $repository;
    }

    protected function applyCartCreated(CartCreated $event): void
    {
        $cartView = CartView::fromSerializable($event);

        $this->repository->add($cartView);
    }

    protected function applyProductAddedToCart(ProductAddedToCart $event): void
    {
        $cartView = $this->repository->oneById($event->uuid);
        $cartProduct = $event->cartProduct->toArray();
        $products = $cartView->products();
        $productId = $event->cartProduct->toArray()['productId'];

        if (isset($products[$productId])) {
            $products[$productId]['quantity'] += $cartProduct['quantity'];
        } else {
            $products[$productId] = $cartProduct;
        }

        $cartView->changeProducts($products);
        $cartView->changeUpdatedAt($event->updatedAt);

        $this->repository->apply();
    }

    protected function applyCartProductChangedQuantity(CartProductChangedQuantity $event): void
    {
        $cartView = $this->repository->oneById($event->uuid);
        $cartProduct = $event->cartProduct->toArray();
        $products = $cartView->products();
        $productId = $cartProduct['productId'];

        if ($event->increase) {
            $products[$productId]['quantity'] += $cartProduct['quantity'];
        } else {
            // if decreasing quantity is bigger than actual product quantity we set product quantity to 0
            if ($cartProduct['quantity'] > $products[$productId]['quantity']) {
                $products[$productId]['quantity'] = 0;
            } else {
                $products[$productId]['quantity'] -= $event->cartProduct->toArray()['quantity'];
            }
        }

        $cartView->changeProducts($products);
        $cartView->changeUpdatedAt($event->updatedAt);

        $this->repository->apply();
    }

    protected function applyCartProductRemoved(CartProductRemoved $event): void
    {
        $cartView = $this->repository->oneById($event->uuid);

        $products = $cartView->products();
        unset($products[$event->cartProduct->toArray()['productId']]);

        $cartView->changeProducts($products);
        $cartView->changeUpdatedAt($event->updatedAt);

        $this->repository->apply();
    }

    protected function applyCartDeleted(CartDeleted $event): void
    {
        $cartView = $this->repository->oneById($event->uuid);

        $this->repository->delete($cartView);
    }

    protected function applyCartConfirmed(CartConfirmed $event): void
    {
        $cartView = $this->repository->oneById($event->uuid);

        $cartView->changeStatus($event->status);

        $this->repository->apply();
    }
}
