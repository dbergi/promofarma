<?php

declare(strict_types=1);

namespace App\Infrastructure\Cart\Query\Mysql;

use App\Domain\Cart\Repository\CartReadModelRepositoryInterface;
use App\Infrastructure\Cart\Query\Projections\CartView;
use App\Infrastructure\Shared\Query\Repository\MysqlRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Ramsey\Uuid\UuidInterface;

final class MysqlCartReadModelRepository extends MysqlRepository implements CartReadModelRepositoryInterface
{

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->class = CartView::class;
        parent::__construct($entityManager);
    }

    /**
     * @param UuidInterface $id
     * @return CartView
     * @throws NonUniqueResultException
     */
    public function oneById(UuidInterface $id): CartView
    {
        $qb = $this->repository
            ->createQueryBuilder('cart')
            ->where('cart.uuid = :id')
            ->setParameter('id', $id->getBytes())
        ;

        return $this->oneOrException($qb);
    }

    public function add(CartView $cart): void
    {
        $this->register($cart);
    }

    public function delete(CartView $cart): void
    {
        $this->remove($cart);
    }
}
