<?php


namespace App\Infrastructure\Cart\Query\Projections;


use App\Domain\Shared\Exception\DateTimeException;
use App\Domain\Shared\ValueObject\DateTime;
use App\Domain\Shared\ValueObject\Status;
use Broadway\ReadModel\SerializableReadModel;
use Broadway\Serializer\Serializable;

class CartView implements SerializableReadModel
{
    private string $uuid;

    private array $products;

    private DateTime $createdAt;

    private ?DateTime $updatedAt;

    private Status $status;

    /**
     * @param Serializable $event
     * @return CartView
     * @throws DateTimeException
     */
    public static function fromSerializable(Serializable $event): self
    {
        return self::deserialize($event->serialize());
    }

    /**
     * @param array $data
     * @return CartView
     * @throws DateTimeException
     * @throws DateTimeException
     */
    public static function deserialize(array $data): self
    {
        $instance = new self();

        $instance->uuid = $data['uuid'];
        $instance->products = $data['products'];
        $instance->createdAt = DateTime::fromString($data['created_at']);
        $instance->updatedAt = isset($data['updated_at']) ? DateTime::fromString($data['updated_at']) : null;

        $instance->status = $data['status'];

        return $instance;
    }

    public function serialize(): array
    {
        return [
            'uuid' => $this->getId(),
            'products' => $this->products(),
        ];
    }

    public function getId(): string
    {
        return $this->uuid;
    }

    public function products(): array
    {
        return $this->products;
    }

    public function changeProducts(array $products): void
    {
        $this->products = $products;
    }

    public function changeUpdatedAt(DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function changeStatus(Status $status): void
    {
        $this->status = $status;
    }
}