<?php


namespace App\Infrastructure\Cart\Repository;


use App\Domain\Cart\Cart;
use Broadway\EventHandling\EventBus;
use Broadway\EventSourcing\AggregateFactory\PublicConstructorAggregateFactory;
use Broadway\EventSourcing\EventSourcingRepository;
use Broadway\EventStore\EventStore;
use Ramsey\Uuid\UuidInterface;

final class CartStore extends EventSourcingRepository
{

    public function __construct(
        EventStore $eventStore,
        EventBus $eventBus,
        array $eventStreamDecorators = []
    )
    {
        parent::__construct(
            $eventStore,
            $eventBus,
            Cart::class,
            new PublicConstructorAggregateFactory(),
            $eventStreamDecorators
        );
    }

    public function get(UuidInterface $id): Cart
    {
        /** @var Cart $cart */
        $cart = $this->load($id->toString());

        return $cart;
    }

    public function store(Cart $cart): void
    {
        $this->save($cart);
    }
}