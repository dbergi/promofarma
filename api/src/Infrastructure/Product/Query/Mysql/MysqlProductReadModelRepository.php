<?php

declare(strict_types=1);

namespace App\Infrastructure\Product\Query\Mysql;

use App\Domain\Product\Repository\ProductReadModelRepositoryInterface;
use App\Infrastructure\Product\Query\Projections\ProductView;
use App\Infrastructure\Shared\Query\Repository\MysqlRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Ramsey\Uuid\UuidInterface;

final class MysqlProductReadModelRepository extends MysqlRepository implements ProductReadModelRepositoryInterface
{

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->class = ProductView::class;
        parent::__construct($entityManager);
    }

    /**
     * @param UuidInterface $id
     * @return ProductView
     * @throws NonUniqueResultException
     */
    public function oneById(UuidInterface $id): ProductView
    {
        $qb = $this->repository
            ->createQueryBuilder('product')
            ->where('product.uuid = :id')
            ->setParameter('id', $id->getBytes())
        ;

        return $this->oneOrException($qb);
    }

    public function allBySellerId(UuidInterface $sellerId): array
    {
         return $this->repository
            ->createQueryBuilder('product')
            ->where('product.seller = :id')
            ->setParameter('id', $sellerId->getBytes())
             ->getQuery()
             ->getResult()
        ;
    }

    public function add(ProductView $product): void
    {
        $this->register($product);
    }

    public function delete(ProductView $product): void
    {
        $this->remove($product);
    }
}
