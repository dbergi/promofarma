<?php

declare(strict_types=1);

namespace App\Infrastructure\Product\Query;

use App\Domain\Product\Event\ProductCreated;
use App\Domain\Product\Event\ProductReactivated;
use App\Domain\Product\Event\ProductRemoved;
use App\Domain\Seller\Event\ProductRemovedFromSeller;
use App\Domain\Seller\Event\SellerDeleted;
use App\Domain\Shared\ValueObject\Status;
use App\Infrastructure\Product\Query\Mysql\MysqlProductReadModelRepository;
use App\Infrastructure\Product\Query\Projections\ProductView;
use App\Infrastructure\Seller\Query\Mysql\MysqlSellerReadModelRepository;
use Broadway\ReadModel\Projector;
use Ramsey\Uuid\Uuid;

class ProductProjectionFactory extends Projector
{

    private MysqlProductReadModelRepository $repository;

    private MysqlSellerReadModelRepository $sellerRepository;

    public function __construct(
        MysqlProductReadModelRepository $repository,
        MysqlSellerReadModelRepository $sellerRepository
    ) {
        $this->repository = $repository;
        $this->sellerRepository = $sellerRepository;
    }

    protected function applyProductCreated(ProductCreated $event): void
    {
        $sellerView = $this->sellerRepository->oneById($event->sellerId);

        $productView = ProductView::fromSerializable($event);

        $productView->setSeller($sellerView);

        $this->repository->add($productView);
    }

    protected function applyProductReactivated(ProductReactivated $event): void
    {
        $sellerView = $this->sellerRepository->oneById($event->sellerId);

        $productView = ProductView::fromSerializable($event);

        $productView->setSeller($sellerView);
        $productView->changeUpdatedAt($event->updatedAt);

        $this->repository->add($productView);
    }

    protected function applyProductRemoved(ProductRemoved $event): void
    {
        $productReadModel = $this->repository->oneById($event->uuid);

        $this->repository->delete($productReadModel);
    }
}
