<?php


namespace App\Infrastructure\Product\Query\Projections;


use App\Domain\Shared\Exception\DateTimeException;
use App\Domain\Shared\ValueObject\DateTime;
use App\Domain\Shared\ValueObject\Status;
use App\Infrastructure\Seller\Query\Projections\SellerView;
use Broadway\ReadModel\SerializableReadModel;
use Broadway\Serializer\Serializable;

class ProductView implements SerializableReadModel
{
    private string $uuid;

    private string $name;

    private float $cost;

    private ?SellerView $seller;

    private DateTime $createdAt;

    private ?DateTime $updatedAt;

    private Status $status;

    /**
     * @param Serializable $event
     * @return CartView
     * @throws DateTimeException
     */
    public static function fromSerializable(Serializable $event): self
    {
        return self::deserialize($event->serialize());
    }

    /**
     * @param array $data
     * @return CartView
     * @throws DateTimeException
     * @throws DateTimeException
     */
    public static function deserialize(array $data): self
    {
        $instance = new self();

        $instance->uuid = $data['uuid'];
        $instance->name = $data['name'];
        $instance->cost = $data['cost'];
        $instance->seller = isset($data['seller']) ? $data['seller'] : null;

        $instance->createdAt = DateTime::fromString($data['created_at']);
        $instance->updatedAt = isset($data['updated_at']) ? DateTime::fromString($data['updated_at']) : null;

        $instance->status = $data['status'];

        return $instance;
    }

    public function serialize(): array
    {
        return [
            'uuid' => $this->getId(),
            'name' => $this->name(),
            'cost' => $this->cost(),
            'seller' => $this->seller()->id()->__toString()
        ];
    }

    public function getId(): string
    {
        return $this->uuid;
    }

    public function name(): string
    {
        return (string) $this->name;
    }

    public function cost(): float
    {
        return $this->cost;
    }

    public function seller(): SellerView
    {
        return $this->seller;
    }

    public function setSeller(SellerView $seller): void
    {
        $this->seller = $seller;
    }

    public function changeUpdatedAt(DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function changeStatus(Status $status): void
    {
        $this->status = $status;
    }
}