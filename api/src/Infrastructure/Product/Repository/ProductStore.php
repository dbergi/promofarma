<?php


namespace App\Infrastructure\Product\Repository;


use App\Domain\Product\Product;
use Broadway\EventHandling\EventBus;
use Broadway\EventSourcing\AggregateFactory\PublicConstructorAggregateFactory;
use Broadway\EventSourcing\EventSourcingRepository;
use Broadway\EventStore\EventStore;
use Ramsey\Uuid\UuidInterface;

final class ProductStore extends EventSourcingRepository
{

    public function __construct(
        EventStore $eventStore,
        EventBus $eventBus,
        array $eventStreamDecorators = []
    )
    {
        parent::__construct(
            $eventStore,
            $eventBus,
            Product::class,
            new PublicConstructorAggregateFactory(),
            $eventStreamDecorators
        );
    }

    public function get(UuidInterface $uuid): Product
    {
        /** @var Product $seller */
        $seller = $this->load($uuid->toString());

        return $seller;
    }

    public function store(Product $product): void
    {
        $this->save($product);
    }
}