<?php

declare(strict_types=1);

namespace App\Infrastructure\Seller\Query\Mysql;

use App\Domain\Seller\Repository\SellerReadModelRepositoryInterface;
use App\Infrastructure\Seller\Query\Projections\SellerView;
use App\Infrastructure\Shared\Query\Repository\MysqlRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Ramsey\Uuid\UuidInterface;

final class MysqlSellerReadModelRepository extends MysqlRepository implements SellerReadModelRepositoryInterface
{

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->class = SellerView::class;
        parent::__construct($entityManager);
    }

    /**
     * @param UuidInterface $id
     * @return SellerView
     * @throws NonUniqueResultException
     */
    public function oneById(UuidInterface $id): SellerView
    {
        $qb = $this->repository
            ->createQueryBuilder('seller')
            ->where('seller.uuid = :id')
            ->setParameter('id', $id->getBytes())
        ;

        return $this->oneOrException($qb);
    }

    public function add(SellerView $seller): void
    {
        $this->register($seller);
    }

    public function delete(SellerView $seller): void
    {
        $this->remove($seller);
    }
}
