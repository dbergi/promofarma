<?php

declare(strict_types=1);

namespace App\Infrastructure\Seller\Query\Projections;

use App\Domain\Shared\Exception\DateTimeException;
use App\Domain\Shared\ValueObject\DateTime;
use App\Domain\Shared\ValueObject\Status;
use Broadway\ReadModel\SerializableReadModel;
use Broadway\Serializer\Serializable;
use Doctrine\Common\Collections\Collection;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

class SellerView implements SerializableReadModel
{
    private UuidInterface $uuid;

    private string $name;

    private Collection $products;

    private DateTime $createdAt;

    private ?DateTime $updatedAt;

    private Status $status;

    /**
     * @param Serializable $event
     * @return SellerView
     * @throws DateTimeException
     */
    public static function fromSerializable(Serializable $event): self
    {
        return self::deserialize($event->serialize());
    }

    /**
     * @param array $data
     * @return SellerView
     * @throws DateTimeException
     */
    public static function deserialize(array $data): self
    {
        $instance = new self();

        $instance->uuid =Uuid::fromString($data['id']);
        $instance->name = $data['name'];
        $instance->products = $data['products'];

        $instance->createdAt = DateTime::fromString($data['created_at']);
        $instance->updatedAt = isset($data['updated_at']) ? DateTime::fromString($data['updated_at']) : null;

        $instance->status = new Status($data['status']);

        return $instance;
    }

    public function serialize(): array
    {
        return [
            'uuid' => $this->getId(),
            'name' => $this->name(),
            'products' => $this->products(),
        ];
    }


    public function getId(): string
    {
        return $this->uuid->toString();
    }

    public function id(): UuidInterface
    {
        return $this->uuid;
    }

    public function name(): string
    {
        return (string) $this->name;
    }

    public function products(): Collection
    {
        return $this->products;
    }

    public function setProducts(Collection $products): void
    {
        $this->products = $products;
    }

    public function changeUpdatedAt(DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function changeStatus(Status $status): void
    {
        $this->status = $status;
    }
}
