<?php

declare(strict_types=1);

namespace App\Infrastructure\Seller\Query;

use App\Domain\Seller\Event\DeletedSellerReactivated;
use App\Domain\Seller\Event\ProductAddedToSeller;
use App\Domain\Seller\Event\ProductRemovedFromSeller;
use App\Domain\Seller\Event\SellerAdded;
use App\Domain\Seller\Event\SellerDeleted;
use App\Domain\Shared\Event\StatusChanged;
use App\Domain\Shared\ValueObject\Status;
use App\Infrastructure\Product\Query\Mysql\MysqlProductReadModelRepository;
use App\Infrastructure\Seller\Query\Mysql\MysqlSellerReadModelRepository;
use App\Infrastructure\Seller\Query\Projections\SellerView;
use Broadway\ReadModel\Projector;
use Doctrine\ORM\NonUniqueResultException;

class SellerProjectionFactory extends Projector
{

    private MysqlSellerReadModelRepository $repository;

    private MysqlProductReadModelRepository $productRepository;

    public function __construct(MysqlSellerReadModelRepository $repository, MysqlProductReadModelRepository $productRepository)
    {
        $this->repository = $repository;
        $this->productRepository = $productRepository;
    }

    /**
     * @param SellerAdded $event
     * @throws \App\Domain\Shared\Exception\DateTimeException
     */
    protected function applySellerAdded(SellerAdded $event): void
    {
        $sellerReadModel = SellerView::fromSerializable($event);

        $this->repository->add($sellerReadModel);
    }

    protected function applyDeletedSellerReactivated(DeletedSellerReactivated $event): void
    {
        $sellerReadModel = SellerView::fromSerializable($event);

        $sellerReadModel->changeUpdatedAt($event->updatedAt);

        $this->repository->add($sellerReadModel);
    }

    /**
     * @param SellerDeleted $event
     * @throws NonUniqueResultException
     */
    protected function applySellerDeleted(SellerDeleted $event): void
    {
        /** @var SellerView $sellerReadModel */
        $sellerReadModel = $this->repository->oneById($event->id);

        $this->repository->delete($sellerReadModel);
    }

    protected function applyProductAddedToSeller(ProductAddedToSeller $event): void
    {
        /** @var SellerView $sellerReadModel */
        $sellerReadModel = $this->repository->oneById($event->sellerId);

        $productReadModel = $this->productRepository->oneById($event->productId);

        $products = $sellerReadModel->products();

        $products[$event->productId->toString()] = $productReadModel;

        $sellerReadModel->setProducts($products);
        $sellerReadModel->changeUpdatedAt($event->updatedAt);

        $this->repository->apply();
    }

    protected function applyProductRemovedFromSeller(ProductRemovedFromSeller $event): void
    {
        /** @var SellerView $sellerReadModel */
        $sellerReadModel = $this->repository->oneById($event->sellerId);

        $sellerReadModel->changeUpdatedAt($event->updatedAt);

        $this->repository->apply();
    }
}
