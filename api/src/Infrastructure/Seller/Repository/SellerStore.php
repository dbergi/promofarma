<?php


namespace App\Infrastructure\Seller\Repository;


use App\Domain\Seller\Repository\SellerRepositoryInterface;
use App\Domain\Seller\Seller;
use Broadway\EventHandling\EventBus;
use Broadway\EventSourcing\AggregateFactory\PublicConstructorAggregateFactory;
use Broadway\EventSourcing\EventSourcingRepository;
use Broadway\EventStore\EventStore;
use Ramsey\Uuid\UuidInterface;

final class SellerStore extends EventSourcingRepository
{

    public function __construct(
        EventStore $eventStore,
        EventBus $eventBus,
        array $eventStreamDecorators = []
    ) {
        parent::__construct(
            $eventStore,
            $eventBus,
            Seller::class,
            new PublicConstructorAggregateFactory(),
            $eventStreamDecorators
        );
    }

    public function get(UuidInterface $id): Seller
    {
        /** @var Seller $seller */
        $seller =  $this->load($id->toString());

        return $seller;
    }

    public function store(Seller $seller): void
    {
        $this->save($seller);
    }
}