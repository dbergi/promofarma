<?php


namespace App\Infrastructure\UI\Http\Rest\Controller\Cart;


use App\Application\Command\Cart\AddProductToCartCommand;
use App\Application\Command\Cart\ChangeProductQuantityCommand;
use App\Application\Command\Cart\ConfirmCartCommand;
use App\Application\Command\Cart\DeleteCartCommand;
use App\Application\Command\Cart\RemoveProductFromCartCommand;
use App\Application\Query\Cart\GetTotalAmountOfCartQuery;
use App\Infrastructure\UI\Http\Rest\Controller\CommandQueryController;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Ramsey\Uuid\Uuid;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends CommandQueryController
{
    /**
     * @Route(
     *     "/addCartProduct",
     *     name="add_product_to_cart",
     *     methods={"POST"}
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="A json string with the uuids requested",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="cartId", type="string"),
     *         @SWG\Property(property="productId", type="string")
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Bad request",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Not found",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Parameter(
     *     name="body",
     *     type="json",
     *     in="body",
     *     schema=@SWG\Schema(type="object",
     *         @SWG\Property(property="cartId", type="string", required={"false"}),
     *         @SWG\Property(property="productId", type="string"),
     *         @SWG\Property(property="quantity", type="integer"),
     *     )
     * )
     *
     *  @SWG\Tag(name="Cart")
     *
     * @param Request $request
     * @return JsonResponse
     * @throws AssertionFailedException
     */
    public function addCartProduct(Request $request): JsonResponse
    {
        $cartId = $request->get('cartId')?: Uuid::uuid4()->toString();
        $productId = $request->get('productId');
        $quantity = $request->get('quantity');

        Assertion::uuid($cartId, "'cartId' is not valid");
        Assertion::uuid($productId, "'productId' is not valid");
        Assertion::integer($quantity, "'quantity' mus be valid integer");

        $command = new AddProductToCartCommand(Uuid::fromString($cartId), Uuid::fromString($productId), $quantity);

        $this->exec($command);

        return JsonResponse::create([
            'cartId' => $cartId,
            'productId' => $productId
        ], JsonResponse::HTTP_OK);
    }

    /**
     * @Route(
     *     "/changeCartProductQuantity",
     *     name="change_product_quantity",
     *     methods={"POST"}
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="A json string with the uuids requested",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="cartId", type="string"),
     *         @SWG\Property(property="productId", type="string")
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Bad request",
     *     @SWG\Schema(type="obeject",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Not found",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Parameter(
     *     name="body",
     *     type="json",
     *     in="body",
     *     schema=@SWG\Schema(type="object",
     *         @SWG\Property(property="cartId", type="string"),
     *         @SWG\Property(property="productId", type="string"),
     *         @SWG\Property(property="quantity", type="integer"),
     *         @SWG\Property(property="increase", type="boolean"),
     *     )
     * )
     *
     * @SWG\Tag(name="Cart")
     *
     * @param Request $request
     * @return JsonResponse
     * @throws AssertionFailedException
     */
    public function changeCartProductQuantity(Request $request): JsonResponse
    {
        $cartId = $request->get('cartId');
        $productId = $request->get('productId');
        $quantity = $request->get('quantity');
        $increase = $request->get('increase');

        Assertion::uuid($cartId, "'cartId' is not valid");
        Assertion::uuid($productId, "'productId' is not valid");
        Assertion::integer($quantity, "'quantity' mus be valid integer");
        Assertion::boolean($increase, "'increase' mus be valid boolean");

        $command = new ChangeProductQuantityCommand(Uuid::fromString($cartId), Uuid::fromString($productId), $quantity, $increase);

        $this->exec($command);

        return JsonResponse::create([
            'cartId' => $cartId,
            'productId' => $productId
        ], JsonResponse::HTTP_OK);
    }

    /**
     * @Route(
     *     "/removeCartProduct",
     *     name="remove_cart_product",
     *     methods={"POST"}
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="A json string with the uuids requested",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="cartId", type="string"),
     *         @SWG\Property(property="productId", type="string")
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Bad request",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Not found",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Parameter(
     *     name="body",
     *     type="json",
     *     in="body",
     *     schema=@SWG\Schema(type="object",
     *         @SWG\Property(property="cartId", type="string"),
     *         @SWG\Property(property="productId", type="string")
     *     )
     * )
     *
     * @SWG\Tag(name="Cart")
     *
     * @param Request $request
     * @return JsonResponse
     * @throws AssertionFailedException
     */
    public function removeCartProduct(Request $request): JsonResponse
    {
        $cartId = $request->get('cartId');
        $productId = $request->get('productId');

        Assertion::uuid($cartId, "'cartId' is not valid");
        Assertion::uuid($productId, "'productId' is not valid");

        $command = new RemoveProductFromCartCommand(Uuid::fromString($cartId), Uuid::fromString($productId));

        $this->exec($command);

        return JsonResponse::create([
            'cartId' => $cartId,
            'productId' => $productId
        ], JsonResponse::HTTP_OK);
    }

    /**
     * @Route(
     *     "/deleteCart",
     *     name="delete_Cart",
     *     methods={"POST"}
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="A json string with the uuids requested",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="cartId", type="string")
     *     )
     * )
     * @SWG\Response(
     *     response=400,
     *     description="Bad request",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Not found",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Parameter(
     *     name="body",
     *     type="json",
     *     in="body",
     *     schema=@SWG\Schema(type="object",
     *         @SWG\Property(property="cartId", type="string")
     *     )
     * )
     *
     * @SWG\Tag(name="Cart")
     *
     * @param Request $request
     * @return JsonResponse
     * @throws AssertionFailedException
     */
    public function deleteCart(Request $request): JsonResponse
    {
        $cartId = $request->get('cartId');

        Assertion::uuid($cartId, "'cartId' is not valid");

        $command = new DeleteCartCommand(Uuid::fromString($cartId));

        $this->exec($command);

        return JsonResponse::create([
            'cartId' => $cartId,
        ], JsonResponse::HTTP_OK);
    }

    /**
     * @Route(
     *     "/confirmCart",
     *     name="confirm_Cart",
     *     methods={"POST"}
     * )
     *
     *  @SWG\Response(
     *     response=200,
     *     description="A json string with the uuids requested",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="cartId", type="string")
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Bad request",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Not found",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Parameter(
     *     name="body",
     *     type="json",
     *     in="body",
     *     schema=@SWG\Schema(type="object",
     *         @SWG\Property(property="cartId", type="string")
     *     )
     * )
     *
     * @SWG\Tag(name="Cart")
     *
     * @param Request $request
     * @return JsonResponse
     * @throws AssertionFailedException
     */
    public function confirmCart(Request $request): JsonResponse
    {
        $cartId = $request->get('cartId');

        Assertion::uuid($cartId, "'cartId' is not valid");

        $command = new ConfirmCartCommand(Uuid::fromString($cartId));

        $this->exec($command);

        return JsonResponse::create([
            'cartId' => $cartId,
        ], JsonResponse::HTTP_OK);
    }

    /**
     * @Route(
     *     "/getCartTotalAmount",
     *     name="get_cart_total_amount",
     *     methods={"POST"}
     * )
     *
     *  @SWG\Response(
     *     response=200,
     *     description="A json string with the uuid of the cart and the total amount",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="cartId", type="string"),
     *         @SWG\Property(property="totalAmount", type="number")
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Bad request",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Not found",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Parameter(
     *     name="body",
     *     type="json",
     *     in="body",
     *     schema=@SWG\Schema(type="object",
     *         @SWG\Property(property="cartId", type="string")
     *     )
     * )
     *
     * @SWG\Tag(name="Cart")
     *
     * @param Request $request
     * @return JsonResponse
     * @throws AssertionFailedException
     */
    public function getTotalAmount(Request $request): JsonResponse
    {
        $cartId = $request->get('cartId');

        Assertion::uuid($cartId, "'cartId' is not valid");

        $query = new GetTotalAmountOfCartQuery(Uuid::fromString($cartId));

        $totalAmount = $this->ask($query);

        return JsonResponse::create([
            'cartId' => $cartId,
            'totalAmount' => $totalAmount
        ], JsonResponse::HTTP_OK);
    }
}