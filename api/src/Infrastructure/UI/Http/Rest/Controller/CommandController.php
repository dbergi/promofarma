<?php

declare(strict_types=1);

namespace App\Infrastructure\UI\Http\Rest\Controller;

use Broadway\CommandHandling\CommandBus;

abstract class CommandController
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    public function __construct(CommandBus $commandBus)
    {
        $this->commandBus = $commandBus;
    }

    protected function exec($command): void
    {
        $this->commandBus->dispatch($command);
    }
}
