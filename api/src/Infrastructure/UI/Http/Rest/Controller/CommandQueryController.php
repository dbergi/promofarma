<?php

declare(strict_types=1);

namespace App\Infrastructure\UI\Http\Rest\Controller;

use Broadway\CommandHandling\CommandBus;
use League\Tactician\CommandBus as QueryBus;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class CommandQueryController extends QueryController
{
    /**
     * @var CommandBus
     */
    private $commandBus;

    protected function exec($command): void
    {
        $this->commandBus->dispatch($command);
    }

    public function __construct(CommandBus $commandBus, QueryBus $queryBus, UrlGeneratorInterface $router)
    {
        parent::__construct($queryBus, $router);
        $this->commandBus = $commandBus;
    }
}
