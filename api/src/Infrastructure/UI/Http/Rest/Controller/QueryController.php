<?php

declare(strict_types=1);

namespace App\Infrastructure\UI\Http\Rest\Controller;

use League\Tactician\CommandBus;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

abstract class QueryController
{
    private CommandBus $queryBus;

    private UrlGeneratorInterface $router;

    public function __construct(CommandBus $queryBus, UrlGeneratorInterface $router)
    {
        $this->queryBus = $queryBus;
        $this->router = $router;
    }

    protected function ask($query)
    {
        return $this->queryBus->handle($query);
    }

    protected function route(string $name, array $params = []): string
    {
        return $this->router->generate($name, $params);
    }
}
