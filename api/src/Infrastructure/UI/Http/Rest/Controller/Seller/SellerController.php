<?php


namespace App\Infrastructure\UI\Http\Rest\Controller\Seller;


use App\Application\Command\Seller\AddProductToSellerCommand;
use App\Application\Command\Seller\AddSellerCommand;
use App\Application\Command\Seller\RemoveProductFromSellerCommand;
use App\Application\Command\Seller\RemoveSellerCommand;
use App\Infrastructure\UI\Http\Rest\Controller\CommandController;
use Assert\Assertion;
use Assert\AssertionFailedException;
use Ramsey\Uuid\Uuid;
use Swagger\Annotations as SWG;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SellerController extends CommandController
{

    /**
     * @Route(
     *     "/addSeller",
     *     name="add_seller",
     *     methods={"POST"}
     * )
     *
     *  @SWG\Response(
     *     response=201,
     *     description="A json string with the uuid created",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="sellerId", type="string")
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Bad request",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Not found",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Parameter(
     *     name="body",
     *     type="json",
     *     in="body",
     *     schema=@SWG\Schema(type="object",
     *         @SWG\Property(property="sellerId", type="string", required={"false"}),
     *         @SWG\Property(property="name", type="string")
     *     )
     * )
     *
     *  @SWG\Tag(name="Seller")
     *
     * @param Request $request
     * @return JsonResponse
     * @throws AssertionFailedException
     */
    public function addSeller(Request $request): JsonResponse
    {
        $uuid = $request->get('sellerId')?: Uuid::uuid4()->toString();
        $name = $request->get('name');

        Assertion::notNull($name, "Name can\'t be null");

        $command = new AddSellerCommand(Uuid::fromString($uuid), $name);

        $this->exec($command);

        return JsonResponse::create(['uuid' => $uuid], JsonResponse::HTTP_CREATED);
    }

    /**
     * @Route(
     *     "/deleteSeller",
     *     name="delete_seller",
     *     methods={"POST"}
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="A json string with the uuid requested or created",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="sellerId", type="string")
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Bad request",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Not found",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Parameter(
     *     name="body",
     *     type="json",
     *     in="body",
     *     schema=@SWG\Schema(type="object",
     *         @SWG\Property(property="sellerId", type="string")
     *     )
     * )
     *
     * @SWG\Tag(name="Seller")
     *
     * @param Request $request
     * @return JsonResponse
     * @throws AssertionFailedException
     */
    public function deleteSeller(Request $request): JsonResponse
    {
        $uuid = $request->get('sellerId');

        Assertion::uuid($uuid, "Not valid uuid");

        $command = new RemoveSellerCommand(Uuid::fromString($uuid));

        $this->exec($command);

        return JsonResponse::create(['uuid' => $uuid], JsonResponse::HTTP_OK);
    }

    /**
     * @Route(
     *      "/addSellerProduct",
     *     name="add_seller_product",
     *     methods={"POST"}
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="A json string with the uuid requested or created",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="sellerId", type="string"),
     *         @SWG\Property(property="productId", type="string")
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Bad request",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Not found",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Parameter(
     *     name="body",
     *     type="json",
     *     in="body",
     *     schema=@SWG\Schema(type="object",
     *         @SWG\Property(property="sellerId", type="string"),
     *         @SWG\Property(property="productId", type="string", required={"false"}),
     *         @SWG\Property(property="productName", type="string"),
     *         @SWG\Property(property="productCost", type="number")
     *     )
     * )
     *
     * @SWG\Tag(name="Seller")
     *
     * @param Request $request
     * @return JsonResponse
     * @throws AssertionFailedException
     */
    public function addSellerProduct(Request $request): JsonResponse
    {
        $uuid = $request->get('sellerId');
        $productId = $request->get('productId')?: Uuid::uuid4()->toString();
        $productName = $request->get('productName');
        $productCost = $request->get('productCost');

        Assertion::uuid($uuid, "Not valid uuid");
        Assertion::notNull($productName, "Product name can't be null");
        Assertion::float($productCost, "Cost must be float");

        $command = new AddProductToSellerCommand(Uuid::fromString($uuid), Uuid::fromString($productId), $productName, $productCost);

        $this->exec($command);

        return JsonResponse::create([
            'sellerId' => $uuid,
            'productId' => $productId,
            'productName' => $productName,
            'cost' => $productCost
        ], JsonResponse::HTTP_CREATED);
    }

    /**
     * @Route(
     *      "/removeSellerProduct",
     *     name="remove_seller_product",
     *     methods={"POST"}
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="A json string with the uuid requested or created",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="sellerId", type="string"),
     *         @SWG\Property(property="productId", type="string")
     *     )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Bad request",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Not found",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="errors", type="object",
     *             @SWG\Property(property="title", type="string"),
     *             @SWG\Property(property="detail", type="string"),
     *             @SWG\Property(property="code", type="integer"),
     *             @SWG\Property(property="status", type="integer")
     *         )
     *     )
     * )
     *
     * @SWG\Parameter(
     *     name="body",
     *     type="json",
     *     in="body",
     *     schema=@SWG\Schema(type="object",
     *         @SWG\Property(property="sellerId", type="string"),
     *         @SWG\Property(property="productId", type="string")
     *     )
     * )
     *
     * @SWG\Tag(name="Seller")
     *
     * @param Request $request
     * @return JsonResponse
     * @throws AssertionFailedException
     */
    public function removeSellerProduct(Request $request): JsonResponse
    {
        $uuid = $request->get('sellerId');
        $productId = $request->get('productId');

        Assertion::uuid($uuid, "Not valid uuid");
        Assertion::uuid($productId, "Not valid uuid");

        $command = new RemoveProductFromSellerCommand(Uuid::fromString($uuid), Uuid::fromString($productId));

        $this->exec($command);

        return JsonResponse::create([
            'sellerId' => $uuid,
            'productId' => $productId
        ], JsonResponse::HTTP_OK);
    }
}