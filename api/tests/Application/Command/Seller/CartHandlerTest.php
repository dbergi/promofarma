<?php


namespace App\Tests\Application\Command\Seller;


use App\Application\Command\Cart\AddProductToCartCommand;
use App\Application\Command\Cart\ChangeProductQuantityCommand;
use App\Application\Command\Cart\ConfirmCartCommand;
use App\Application\Command\Cart\DeleteCartCommand;
use App\Application\Command\Cart\RemoveProductFromCartCommand;
use App\Application\Command\Seller\AddProductToSellerCommand;
use App\Application\Command\Seller\AddSellerCommand;
use App\Application\Query\Cart\GetTotalAmountOfCartQuery;
use App\Domain\Cart\Event\CartConfirmed;
use App\Domain\Cart\Event\CartCreated;
use App\Domain\Cart\Event\CartDeleted;
use App\Domain\Cart\Event\CartProductChangedQuantity;
use App\Domain\Cart\Event\CartProductRemoved;
use App\Domain\Cart\Event\ProductAddedToCart;
use App\Domain\Cart\ValueObject\CartProduct;
use App\Domain\Shared\ValueObject\Status;
use App\Tests\Application\ApplicationTestCase;
use App\Tests\Infrastructure\Shared\Event\EventCollectorListener;
use Broadway\Domain\DomainMessage;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @group integration
 */
class CartHandlerTest extends ApplicationTestCase
{
    private UuidInterface $cartId;

    private UuidInterface $sellerId;

    private UuidInterface $productId;

    private EventCollectorListener $eventCollector;

    protected function setUp(): void
    {
        parent::setUp();

        $this->cartId = Uuid::uuid4();

        $this->eventCollector = $this->service(EventCollectorListener::class);

        // Add Seller
        $command = new AddSellerCommand($this->sellerId = Uuid::uuid4(), 'Johny');
        $this->handle($command);

        // Add Product to Seller
        $command = new AddProductToSellerCommand($this->sellerId, $this->productId = Uuid::uuid4(), 'Mesa', 22.2);
        $this->handle($command);

        // Add Product to Cart
        $command = new AddProductToCartCommand($this->cartId, $this->productId, 2);
        $this->handle($command);
    }

    public function test_adds_product_to_cart_should_fire_event(): void
    {
        /** @var DomainMessage[] $events */
        $events = $this->eventCollector->popEvents();

        self::assertCount(5, $events);

        /** @var CartCreated $cartCreated */
        $cartCreated = $events[3]->getPayload();

        /** @var ProductAddedToCart $productAdded */
        $productAdded = $events[4]->getPayload();

        self::assertInstanceOf(CartCreated::class, $cartCreated);
        self::assertInstanceOf(ProductAddedToCart::class, $productAdded);
        self::assertSame($this->cartId->toString(), $cartCreated->uuid->toString());
        self::assertSame($this->productId->toString(), $productAdded->cartProduct->toArray()['productId']);
    }

    public function test_change_quantity_of_product_should_fire_event(): void
    {
        $command = new ChangeProductQuantityCommand($this->cartId, $this->productId, 3, true);
        $this->handle($command);

        /** @var DomainMessage[] $events */
        $events = $this->eventCollector->popEvents();

        self::assertCount(6, $events);

        /** @var CartProductChangedQuantity $changedQuantity */
        $changedQuantity = $events[5]->getPayload();

        self::assertInstanceOf(CartProductChangedQuantity::class, $changedQuantity);
        self::assertSame($this->cartId->toString(), $changedQuantity->uuid->__toString());
        self::assertSame($this->productId->toString(), $changedQuantity->cartProduct->toArray()['productId']);
        self::assertSame(3, $changedQuantity->cartProduct->toArray()['quantity']);
        self::assertSame(true, $changedQuantity->increase);
    }

    public function test_decrease_product_quantity_till_0_should_fire_events(): void
    {
        $command = new ChangeProductQuantityCommand($this->cartId, $this->productId, 2, false);
        $this->handle($command);

        /** @var DomainMessage[] $events */
        $events = $this->eventCollector->popEvents();

        self::assertCount(7, $events);

        /** @var CartProductRemoved $removedProduct */
        $removedProduct = $events[6]->getPayload();

        self::assertInstanceOf(CartProductRemoved::class, $removedProduct);
    }

    public function test_remove_product_from_the_cart_should_fire_event(): void
    {
        // Add New Product to Existing Seller
        $command = new AddProductToSellerCommand($this->sellerId, $productId = Uuid::uuid4(), 'Coche', 44.4);
        $this->handle($command);

        // Add Product to Cart
        $command = new AddProductToCartCommand($this->cartId, $productId, 3);
        $this->handle($command);

        // Remove product
        $command = new RemoveProductFromCartCommand($this->cartId, $productId);
        $this->handle($command);

        /** @var DomainMessage[] $events */
        $events = $this->eventCollector->popEvents();

        self::assertCount(9, $events);

        /** @var CartProductRemoved $removedProduct */
        $removedProduct = $events[8]->getPayload();

        self::assertInstanceOf(CartProductRemoved::class, $removedProduct);
        self::assertSame($productId->toString(), $removedProduct->cartProduct->toArray()['productId']);
    }

    public function test_get_total_amount_of_cart_should_return_correct_value(): void
    {
        $query = new GetTotalAmountOfCartQuery($this->cartId);
        $totalAmount = $this->ask($query);

        self::assertEquals(44.4, $totalAmount);
    }

    public function test_delete_cart_should_fire_event(): void
    {
        $command = new DeleteCartCommand($this->cartId);
        $this->handle($command);

        /** @var DomainMessage[] $events */
        $events = $this->eventCollector->popEvents();

        self::assertCount(7, $events);

        /** @var CartDeleted $deletedCart */
        $deletedCart = $events[6]->getPayload();
        self::assertInstanceOf(CartDeleted::class, $deletedCart);
    }

    public function test_confirm_cart_should_fire_event(): void
    {
        $command = new ConfirmCartCommand($this->cartId);
        $this->handle($command);

        /** @var DomainMessage[] $events */
        $events = $this->eventCollector->popEvents();

        self::assertCount(6, $events);

        /** @var CartConfirmed $confirmedCart */
        $confirmedCart = $events[5]->getPayload();

        self::assertInstanceOf(CartConfirmed::class, $confirmedCart);
        self::assertEquals(Status::CONFIRMED(), $confirmedCart->status);
    }
}