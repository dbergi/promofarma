<?php


namespace App\Tests\Application\Command\Seller;


use App\Application\Command\Seller\AddProductToSellerCommand;
use App\Application\Command\Seller\AddSellerCommand;
use App\Application\Command\Seller\RemoveProductFromSellerCommand;
use App\Application\Command\Seller\RemoveSellerCommand;
use App\Domain\Product\Event\ProductCreated;
use App\Domain\Product\Event\ProductRemoved;
use App\Domain\Seller\Event\ProductAddedToSeller;
use App\Domain\Seller\Event\ProductRemovedFromSeller;
use App\Domain\Seller\Event\SellerAdded;
use App\Domain\Seller\Event\SellerDeleted;
use App\Domain\Shared\ValueObject\Status;
use App\Tests\Application\ApplicationTestCase;
use App\Tests\Infrastructure\Shared\Event\EventCollectorListener;
use Broadway\Domain\DomainMessage;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @group integration
 */
class SellerHandlerTest extends ApplicationTestCase
{
    private UuidInterface $sellerId;

    private UuidInterface $productId;

    private EventCollectorListener $eventCollector;

    protected function setUp(): void
    {
        parent::setUp();

        $this->sellerId = Uuid::uuid4();
        $this->productId = Uuid::uuid4();
        $this->eventCollector = $this->service(EventCollectorListener::class);

        // Add Seller
        $command = new AddSellerCommand($this->sellerId, 'Pepe');
        $this->handle($command);

        // Add Product
        $command = new AddProductToSellerCommand($this->sellerId, $this->productId, 'Chair', '22.2');
        $this->handle($command);
    }

    public function test_adds_seller_should_fire_event(): void
    {
        /** @var DomainMessage[] $events */
        $events = $this->eventCollector->popEvents();

        self::assertCount(3, $events);

        /** @var SellerAdded $sellerAdded */
        $sellerAdded = $events[0]->getPayload();

        self::assertInstanceOf(SellerAdded::class, $sellerAdded);
        self::assertSame($this->sellerId->toString(), $sellerAdded->id->__toString());
        self::assertSame('Pepe', $sellerAdded->name);
    }

    public function test_add_product_should_fire_events(): void
    {
        /** @var DomainMessage[] $events */
        $events = $this->eventCollector->popEvents();

        self::assertCount(3, $events);

        /** @var ProductCreated $productCreated */
        $productCreated = $events[1]->getPayload();

        /** @var ProductAddedToSeller $productAdded */
        $productAdded = $events[2]->getPayload();

        self::assertInstanceOf(ProductCreated::class, $productCreated);
        self::assertInstanceOf(ProductAddedToSeller::class, $productAdded);
        self::assertSame($this->productId->toString(), $productAdded->productId->toString());
        self::assertSame($this->sellerId->toString(), $productAdded->sellerId->toString());
    }

    public function test_remove_product_should_fire_event(): void
    {
        $command = new RemoveProductFromSellerCommand($this->sellerId, $this->productId);
        $this->handle($command);

        /** @var DomainMessage[] $events */
        $events = $this->eventCollector->popEvents();

        self::assertCount(5, $events);

        /** @var ProductRemovedFromSeller $productRemoved */
        $productRemoved = $events[3]->getPayload();

        self::assertInstanceOf(ProductRemovedFromSeller::class, $productRemoved);
        self::assertSame($this->productId->toString(), $productRemoved->productId->toString());
        self::assertSame($this->sellerId->toString(), $productRemoved->sellerId->toString());
        self::assertEquals(Status::INACTIVE(), $productRemoved->status);
    }

    public function test_remove_seller_should_fire_event(): void
    {
        $command = new RemoveSellerCommand($this->sellerId);
        $this->handle($command);

        /** @var DomainMessage[] $events */
        $events = $this->eventCollector->popEvents();

        self::assertCount(6, $events);

        /** @var ProductRemovedFromSeller $productRemovedFromSeller */
        $productRemovedFromSeller = $events[3]->getPayload();

        /** @var ProductRemoved $productRemoved */
        $productRemoved = $events[4]->getPayload();

        /** @var SellerDeleted $sellerDeleted */
        $sellerDeleted = $events[5]->getPayload();

        self::assertInstanceOf(SellerDeleted::class, $sellerDeleted);
        self::assertInstanceOf(ProductRemovedFromSeller::class, $productRemovedFromSeller);
        self::assertInstanceOf(ProductRemoved::class, $productRemoved);
        self::assertEquals(Status::INACTIVE(), $sellerDeleted->status);
    }
}