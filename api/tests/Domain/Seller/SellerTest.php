<?php


namespace App\Tests\Domain\Seller;


use App\Domain\Product\Product;
use App\Domain\Seller\Seller;
use App\Domain\Shared\Query\Exception\NotFoundException;
use PHPUnit\Framework\TestCase;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * @group unit
 */
class SellerTest extends TestCase
{
    protected UuidInterface $sellerId;

    protected Seller $seller;

    protected Product $product;

    public function setUp(): void
    {
        parent::setUp();

        $this->sellerId = Uuid::uuid4();
        $this->seller = Seller::create($this->sellerId, 'John');
        $this->product = Product::create(Uuid::uuid4(), 'Chair', 22.2, $this->sellerId);
        $this->seller->addProduct($this->product);
    }

    public function test_seller_addProduct_with_empty_data(): void
    {
        $this->expectException(\TypeError::class);
        $this->seller->addProduct();
    }

    public function test_seller_addProduct_with_valid_product(): void
    {
        $products = $this->seller->products();

        $this->assertCount(1, $products);
    }

    public function test_seller_removeProduct_with_no_product(): void
    {
        $this->expectException(\TypeError::class);
        $this->seller->removeProduct();
    }

    public function test_seller_removeProduct_with_valid_product(): void
    {
        $this->seller->removeProduct($this->product);

        $this->assertEmpty($this->seller->products());
    }

    public function test_seller_removeProduct_with_not_linked_product(): void
    {
        $seller = Seller::create($this->sellerId, 'Paul');
        $product = Product::create(Uuid::uuid4(), 'Wheel', 22.2, $seller->id());

        $this->expectException(NotFoundException::class);

        $this->seller->removeProduct($product);
    }
}